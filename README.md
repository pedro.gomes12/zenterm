# Zenterm

ZenDesk on terminal 

# Instalation

## Clone the repository

```bash
git clone git@gitlab.com:pedro.gomes12/zenterm.git
```

## Enter the folder 

```bash
cd zenterm
```
## Create the app.ini file 

```bash
vim app.ini
```

## Change the app.ini file with your credentials 

```ini
[security]
path = https://<my_company>.zendesk.com/api/v2
user = <my_email@my_company.com>
password = <password>
```

## Have fun 

```bash
python3 zenterm.py -h
```
