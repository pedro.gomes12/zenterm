import configparser as cp
import os

settings = cp.ConfigParser()
settings.read(os.path.join(os.path.dirname(__file__), 'app.ini'))
auth = settings['security']



class Auth_zen:
  '''This class function is to authenticate the user'''
  path_api = auth['path']
  user_api = auth['user']
  password_api = auth['password']

  def __init__(self):
     pass


  def path(self):
    return self.path_api
  
  def user(self):
    return self.user_api

  def password(self):
    return self.password_api

  
  def credentials(self):
    auth_str = "('{0}','{1}')".format(self.user,self.password)
    return auth_str
    









  