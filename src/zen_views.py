from auth import Auth_zen
from src.zen_api import Api
from src.zen_users import Users

user = Users()
api = Api()
auth = Auth_zen()

class Views:

    """ views_api() , display_views(), show_specific_views()"""

    def __init__(self):
      pass

   

    def views_api(self):
        res = api.api_call('/views')
        views = res['views']
        return views
    
    def display_views(self,count):
        views_count = api.api_call('/views/count')['count']['value']
        views = api.api_call('/views')
        for i in range (0,views_count):
            view = views['views'][i]
            if count:
                ticket_count = api.api_call('/views/{0}/count'.format(view['id']))['view_count']['value']
                print(view['id'] , view['title'] , '(' , ticket_count ,')')
            else:
                print(view['id'] , view['title'])



    def show_specific_view(self,view_id):
        """Show view based on id.
        this function will print the rusult and not only return it
        """
        
        view = api.api_call('/views/{0}/execute'.format(view_id))
        sv = view['rows']
        i = 0 
        inR = True
        print('++++++++++++++++++++++++++++++++')
        while inR:
            try:
                ticket = sv[i]
                print(ticket['created'])
                print(ticket['ticket']['id'])
                print(ticket['ticket']['subject'])
                print(ticket['ticket']['last_comment']['body'])
                print('--------------')
                i += 1
            except:
                inR = False
                print('++++++++++++++++++++++++++++++++')
        return(view)
