 
from auth import Auth_zen
import requests as req

auth = Auth_zen()

class Api: 

     def __init__(self):
       pass

     def api_call(self, endpoint):
        path = auth.path()
        user = auth.user()
        password = auth.password()       
        api_call = req.get(path + endpoint , auth=(user,password)).json()
        return api_call