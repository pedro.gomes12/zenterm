from auth import Auth_zen
from src.zen_api import Api
from src.zen_users import Users

user = Users()
api = Api()
auth = Auth_zen()

class Tickets: 
    """ tickets_api() , ticket()"""

    def __init__(self):
    
      pass


    def tickets_api(self):
        tickets = api.api_call('/tickets')
        return tickets
    
    def ticket(self,ticket_id):
        ticket = api.api_call('/tickets/{0}'.format(ticket_id))
        return ticket['ticket']

    def tickets_assign_to_user(self,user_id):
        ticekts = api.api_call('/users/{0}/tickets/assigned'.format(user_id))
        return ticekts
       


