from app import App_termzen
import argparse

 
parser = argparse.ArgumentParser(usage=argparse.SUPPRESS)
parser._optionals.title = 'How to use '
parser.add_argument('-v', '--views',help='show views or specic view', nargs='?', const='', metavar='')
parser.add_argument('-t', '--ticket', type=int, help='insert ticket number',metavar='')
parser.add_argument('-c', '--count', help='count tickets per view', action='store_true')
parser.add_argument('-u', '--user',  help='show only tickets related to this users', nargs='?', const='', metavar='')
args = parser.parse_args()


#initialize app.py 
termzen = App_termzen(args)
#Views display logic 
termzen.views()
#Ticket display logic 
termzen.ticket()
#User display logic
termzen.user()














